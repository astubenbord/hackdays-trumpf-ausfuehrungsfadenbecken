FROM ubuntu:20.04

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y --no-install-recommends tzdata keyboard-configuration

# Install libraries
RUN apt-get update && apt-get install -y cmake software-properties-common git build-essential linux-libc-dev libxerces-c3.2 libboost-all-dev libeigen3-dev libflann-dev build-essential libgl1-mesa-dev libglew-dev libsdl2-dev libsdl2-image-dev libglm-dev libfreetype6-dev libvtk6-dev libvtk6-qt-dev

# get OpenCV
RUN git clone https://github.com/opencv/opencv.git \ 
   && cd opencv \
   && git checkout 4.2.0 \
   && mkdir build && cd build

RUN apt-get install -y caffe-cpu

# build OpenCV
RUN cd opencv/build/ \
   && cmake .. \
   && make -j 12 \
   && make install

# # get PCL
RUN cd /opt \
   && git clone https://github.com/PointCloudLibrary/pcl.git pcl-trunk \
   && ln -s /opt/pcl-trunk /opt/pcl \
   && cd /opt/pcl && git checkout pcl-1.9.1 \
   && mkdir -p /opt/pcl-trunk/release

# # build PCL
RUN cd /opt/pcl/release && cmake -DCMAKE_BUILD_TYPE=None -DBUILD_GPU=ON -DBUILD_apps=ON -DWITH_QT=OFF .. \
   && cd /opt/pcl/release && make -j 12 \
   && cd /opt/pcl/release && make install

# # get E57Converter
RUN git clone --recursive https://github.com/DoktorBotti/E57Converter.git

# # build E57Converter
RUN cd E57Converter && \
   mkdir build && \
   cd build && \
   cmake -DOpenCV_DIR="/opencv/build" .. &&\
   make -j 12 && \
   make install

# add server code
COPY CppPointCloud/ /CppPointCloud

RUN cd CppPointCloud && \
   mkdir build && \
   cd build && \
   cmake .. && \
   make -j 3

EXPOSE 8080

RUN apt-get install -y python3-venv 

COPY start_server.sh /start_server.sh
RUN chmod +x /start_server.sh

RUN mkdir /usr/share/data && \
   cp CppPointCloud/examples/example.pcd /usr/share/data/input.pcd

# add api code
COPY rest-api/ /rest-api

RUN cd /rest-api && \
   python3 -m venv apienv && \
   . apienv/bin/activate && \
   pip install -r requirements.txt

COPY /python2DProcessing /python2d/

RUN cd /python2d && \
   python3 -m venv 2dproenv && \
   . 2dproenv/bin/activate && \
   pip install opencv-python numpy argparse

# Entrypoint
ENTRYPOINT [ "/start_server.sh" ]