This folder contains the rather large point cloud processing code and compatibility scripts

## Making e57 compatible
Its the E57Converters' job to transfer the filetype of .e57 to .pcd in two steps. Each step takes multiple parameters:
    ./E57Converter -convert -src "$HOME/Downloads/original.e57" -dst "$HOME/Downloads/pcdOut4/" -res 5 -minRGB 0 -min "-100 -100 -100" -max "100 100 100" -samplePercent 0.125
it is important, that the output folder does not exist yet. A meaningful explanation of the parameters can be found [here](https://github.com/dogod621/E57Converter)
    ./E57Converter -convert -dst "$HOME/Downloads/pcdOut4.pcd" -src "$HOME/Downloads/pcdOut4/" -voxelUnit 0.05 -searchRadiusNumVoxels 6
this second step generates voxels and hereby downsamples again.

## Using PointCloudLibrary
there are multiple ways of doing this. for example you could just build a regular executable. Otherwise we can use the generated tools in ./lib/pcb/build/bin
In this project we will use a combination


cd ~/hackathon/hackdays-trumpf-ausfuehrungsfadenbecken/CppPointCloud/lib/E57Converter/ && rm -rf build && mkdir build && cd build && cmake -DOpenCV_DIR="~/hackathon/hackdays-trumpf-ausfuehrungsfadenbecken/CppPointCloud/lib/opencv/build/" .. && make -j 3 && ./E57Converter/E57Converter -convert -dst "$HOME/Downloads/pcdOut5.pcd" -src "$HOME/Downloads/pcdOut5/" -voxelUnit 0.05 -searchRadiusNumVoxels 6 -meanK 1

cd ~/hackathon/hackdays-trumpf-ausfuehrungsfadenbecken/CppPointCloud/lib/E57Converter/build && cmake -DOpenCV_DIR="~/hackathon/hackdays-trumpf-ausfuehrungsfadenbecken/CppPointCloud/lib/opencv/build/" .. && make -j 3 && ./E57Converter/E57Converter -convert -dst "$HOME/Downloads/pcdOut5.pcd" -src "$HOME/Downloads/pcdOut5/" -voxelUnit 0.05 -searchRadiusNumVoxels 6 -meanK 1

E57Converter/build/E57Converter/E57Converter -convert -dst "$HOME/Downloads/pcdOut5.pcd" -src "$HOME/Downloads/pcdOut5/" -voxelUnit 0.05 -searchRadiusNumVoxels 6 -meanK 1