# General
- Split into two parts: voice and functionality explanation
- Voice part as interview or straight talking into camera
- Use b-rolls to visualize the solution / advantages
- Each b-roll shot ~10 seconds
- Record voice part first, build rest around it
- Story telling
- Most important points in the first 30 seconds
    Who you are, what you do, your solution and advantages

## Framing
- Landscape Mode
- Person in the middle or at 1/3 of picture and face above the middle
- Camera at eye level
- Interviewer close to camera (so that the interviewee looks close or into the camera)
- Dont use zoom of the camera
- Easy, controllable background


## Sound
- Small room, not outside
- Curtains etc can reduce echo

## Video
- Steady shot (Tripod or improvised solutions)
- Film in flight mode
- Screen recordings for program functionality
- Film only one shot of the voice part, cut the bad/ not needed parts later
- Hold eye contact with camera
